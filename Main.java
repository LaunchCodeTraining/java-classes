import javax.sound.midi.SysexMessage;

import classes.Person;
import classes.Politician;
import classes.Trump;

public class Main {

    public static void main(String[] args) {

        // Person examplePerson = new Person("Tom", "White", 25);
        // System.out.println(examplePerson.getFirstName());
        // System.out.println(examplePerson.getLastName());
        // examplePerson.setFirstName("Jill");
        // System.out.println(examplePerson.getFirstName());
        // System.out.println(examplePerson.getAge());
        // examplePerson.haveBirthday();
        // System.out.println(examplePerson.getAge());

        // Politician inherits from Person
        // Politician examplePolitician = new Politician("Tom", "White", 55, "democrat");
        
        // System.out.println(examplePolitician.getFirstName());
        // System.out.println(examplePolitician.getAge());
        // examplePolitician.haveBirthday();
        // System.out.println(examplePolitician.getAge());

        // System.out.println(examplePolitician.getPartyAffiliation());
        // examplePolitician.setPartyAffiliation("republican");
        // System.out.println(examplePolitician.getPartyAffiliation());

        Trump newTrump = new Trump("Donald", "Trump", 75, "republican", "realDonaldTrump");

        System.out.println(newTrump.getFirstName());
        System.out.println(newTrump.getLastName());
        System.out.println(newTrump.getPartyAffiliation());
        System.out.println(newTrump.getTwitterHandle());
        newTrump.sendTweet("covfefe");


        

    }
    
}
       