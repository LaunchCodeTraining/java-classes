package classes;

public abstract class Person {

    // define our properties
    private String firstName;
    private String lastName;
    private int age;

    // constructor
    public Person(String someFirstName, String someLastName, int someAge) {
        this.firstName = someFirstName;
        this.lastName = someLastName;
        this.age = someAge;
    }

    // getters()

    public String getFirstName() {
        return this.firstName;
    }
    
    public String getLastName() {
        return this.lastName;
    }

    public int getAge() {
        return this.age;
    }

    // setters()

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    // class methods
    public void haveBirthday() {
        this.age += 1;
    }


}
