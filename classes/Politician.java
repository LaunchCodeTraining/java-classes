package classes;

public class Politician extends Person {

    // define the properties
    private String partyAffiliation;

    // constructor
    public Politician(String firstName, String lastName, int age, String partyAffiliation) {
        super(firstName, lastName, age);
        this.partyAffiliation = partyAffiliation;
    }

    // getters()
    public String getPartyAffiliation() {
        return this.partyAffiliation;
    }

    // setters()
    public void setPartyAffiliation(String newParty) {
        this.partyAffiliation = newParty;
    }
    
}
