package classes;

import interfaces.Tweeter;

public class Trump extends Politician implements Tweeter {
    String twitterHandle;

    public Trump(String firstName, String lastName, int age, String partyAffiliation, String twitterHandle) {
        super(firstName, lastName, age, partyAffiliation);
        this.twitterHandle = twitterHandle;
    }

    // getters()
    public String getTwitterHandle() {
        return this.twitterHandle;
    }

    // setters()
    public void setTwitterHandle(String newTwitterHandle) {
        this.twitterHandle = newTwitterHandle;
    }

    public void sendTweet(String message) {
        System.out.println(message);
    }
}
