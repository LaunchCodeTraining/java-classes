package interfaces;

public interface Tweeter {
    String getTwitterHandle();
    void sendTweet(String message);
}
